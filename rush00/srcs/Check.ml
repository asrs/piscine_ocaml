(* Check.ml *)

let checkSameChar map a b c =
    (Cell.getChar (List.nth map a)) = (Cell.getChar (List.nth map b)) &&
    (Cell.getChar (List.nth map b)) = (Cell.getChar (List.nth map c)) &&
    (Cell.getChar (List.nth map a)) <> "-"

let checkMapRows map =
    (checkSameChar map 0 1 2) || (checkSameChar map 3 4 5) || (checkSameChar map 6 7 8)

let checkMapDiag map =
    (checkSameChar map 0 4 8) || (checkSameChar map 2 4 6)

let checkMapColumns map =
    (checkSameChar map 0 3 6) || (checkSameChar map 1 4 7) || (checkSameChar map 2 5 8)

let rec checkMapFull map =
    match map with
    | [] -> true
    | head :: tail ->
            if (Cell.getChar head) = "-" then false
            else checkMapFull tail

let checkMapFinished map = (checkMapColumns map) || (checkMapRows map) || (checkMapDiag map) || (checkMapFull map)

(*----------------------------------------------------------------------------*)

let checkSameWin board a b c =
    (Map.getFinish (List.nth board a)) = (Map.getFinish (List.nth board b)) &&
    (Map.getFinish (List.nth board b)) = (Map.getFinish (List.nth board c)) &&
    (Map.getFinish (List.nth board a)) <> "-"

let checkBoardRows board =
    (checkSameWin board 0 1 2) || (checkSameWin board 3 4 5) || (checkSameWin board 6 7 8)

let checkBoardDiag board =
    (checkSameWin board 0 4 8) || (checkSameWin board 2 4 6)

let checkBoardColumns board =
    (checkSameWin board 0 3 6) || (checkSameWin board 1 4 7) || (checkSameWin board 2 5 8)

let rec checkBoardFull board =
    match board with
    | [] -> true
    | head :: tail ->
            if (Map.getFinish head) = "-" then false
            else checkBoardFull tail

let checkBoardFinished board =
    (checkBoardColumns board) || (checkBoardRows board) || (checkBoardDiag board) || (checkBoardFull board)

(*----------------------------------------------------------------------------*)

let equality board =
    if (checkBoardFull board) = true &&
    ((checkBoardColumns board) || (checkBoardRows board) || (checkBoardDiag board)) = false
    then true
    else false
