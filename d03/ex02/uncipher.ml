
let ft_unrot c =
    if (c >= 'a' || c >= 'A') && (c <= 'z' || c <= 'Z') then (
        if (c = 'a' || c = 'A') then
            char_of_int ((int_of_char c) + 25)
        else
            char_of_int ((int_of_char c) - 1)
        )
    else c

let uncaesar n s =
    let rec loop n s =
        if n > 0 then
            loop (pred n) (String.map ft_unrot s)
        else s
    in
    loop n s

let unrot42 s = uncaesar 42 s

let xor n s =
    let do_xor c =
        char_of_int ((int_of_char c) lxor n)
    in
    String.map do_xor s

let rec ft_uncrypt s lst =
    match lst with
    | [] -> s
    | h :: t -> ft_uncrypt (h s) t

let () =
    print_endline (xor 87 "98abcdefghijklmno~PQRSTUVWXYZ0987");
    print_endline "";
    print_string "Unrot42: ";
    print_endline (unrot42 "98abcdefghijklmno~PQRSTUVWXYZ0987");
    print_string "UnCaesar 95: ";
    print_endline (uncaesar 95 "98abcdefghijklmno~PQRSTUVWXYZ0987");
    print_endline "";
    let str = "HINOLMBC@AFGDEZ[X^_\\]RSPKhinolmbc`afgdez{x~|}rspk"
    in
        let decrypted = (ft_uncrypt str
            [(xor (42));
            (uncaesar (11));
            unrot42])
    in
        print_endline ("str:       " ^ str);
        print_endline ("decrypted: " ^ decrypted);
        print_endline ""
