
let ft_rot c =
    if (c >= 'a' || c >= 'A') && (c <= 'z' || c <= 'Z') then (
        if (c = 'z' || c = 'Z') then
            char_of_int ((int_of_char c) - 25)
        else
            char_of_int ((int_of_char c) + 1)
        )
    else c

let caesar n s =
    let rec loop n s =
        if n > 0 then
            loop (pred n) (String.map ft_rot s)
        else s
    in
    loop n s

let rot42 s = caesar 42 s

let xor n s =
    let do_xor c =
        char_of_int ((int_of_char c) lxor n)
    in
    String.map do_xor s

let rec ft_crypt s lst =
    match lst with
    | [] -> s
    | h :: t -> ft_crypt (h s) t

let () =
    print_endline (xor 87 "98abcdefghijklmno~PQRSTUVWXYZ0987");
    print_endline "";
    print_string "Rot42: ";
    print_endline (rot42 "98abcdefghijklmno~PQRSTUVWXYZ0987");
    print_string "Caesar 95: ";
    print_endline (caesar 95 "98abcdefghijklmno~PQRSTUVWXYZ0987");
    print_endline "";
    let str = "abcdefghijklmnopqstuvwxyzABCDEFGHIJKLMNOPQSTUVWXYZ"
    in
        let crypted = (ft_crypt str
            [rot42;
            (caesar (11));
            (xor (42))])
    in
        print_endline ("str:       " ^ str);
        print_endline ("crypted:   " ^ crypted);
        print_endline ""
