type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let rec size tree =
    match tree with
    | Nil -> 0
    | Node (_, left, right) -> 1 + (size left) + (size right)

let rec height tree =
    match tree with
    | Nil -> 0
    | Node (_, left, right) -> 1 + (max (size left) (size right))

let draw_square x y size =
    let half = size / 2
    in
    Graphics.moveto (x - half) (y - half);
    Graphics.lineto (x + half) (y - half);
    Graphics.lineto (x + half) (y + half);
    Graphics.lineto (x - half) (y + half);
    Graphics.lineto (x - half) (y - half)

let draw_box x y size text =
    let z = (size / 2)
    in
    draw_square x y size;
    Graphics.moveto (x - (z / 2)) (y - (z / 6));
    Graphics.draw_string text;
    Graphics.moveto (x + z) y

let draw_plug x y len space sep =
    let half = len / 2
    in
    Graphics.moveto x (y - half);
    Graphics.lineto (x - sep) (y - (space - half));
    Graphics.moveto x (y - half);
    Graphics.lineto (x + sep) (y - (space - half))

let draw_tree tree =
    let len = 30
    in
    let rec loop x y len tree =
        let space = (len * 2) + ((height tree) * 4)
        and sep = len + ((size tree) * (height tree * 3))
        in
        match tree with
        | Node (v, left, right) -> (
            draw_box x y len v;
            draw_plug x y len space sep;
            loop (x + sep) (y - space) len left;
            loop (x - sep) (y - space) len right;
        )
        | Nil -> draw_box x y len "Nil"
    in
    loop 600 1000 len tree

let () =
    let one = Node ("G", Nil, Nil) in
    let two = Node ("H", one, Nil) in
    let three = Node ("A", (Node ("B", Nil, Nil)), (Node ("C", Node ("D", Nil, two), Nil))) in
    let tree = Node ("42", three, two)
    in
    Graphics.open_graph (" "^"1200"^"x"^"1200");
    draw_tree tree;
    ignore (Graphics.read_key ())
