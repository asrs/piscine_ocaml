type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let draw_square x y size =
    let half = size / 2
    in
    Graphics.moveto (x - half) (y - half);
    Graphics.lineto (x + half) (y - half);
    Graphics.lineto (x + half) (y + half);
    Graphics.lineto (x - half) (y + half);
    Graphics.lineto (x - half) (y - half)

let draw_box x y size text =
    let z = (size / 2)
    in
    draw_square x y size;
    Graphics.moveto (x - (z / 2)) (y - (z / 6));
    Graphics.draw_string text;
    Graphics.moveto (x + z) y

let draw_plug x y size space =
    let half = size / 2
    in
    Graphics.moveto (x + half) y;
    Graphics.lineto ((x - half) + space) (y + size);
    Graphics.moveto (x + half) y;
    Graphics.lineto ((x - half) + space) (y - size)

let draw_tree_node  x y size (f : 'a -> string) tree =
    let space = size * 2
    in
    let rec loop x y size tree =
        match tree with
        | Node (v, left, right) -> (
            draw_box x y size (f v);
            draw_plug x y size space;
            loop (x + space) (y + size) size left;
            loop (x + space) (y - size) size right;
        )
        | Nil -> draw_box x y size "Nil"
    in
    loop (x - size) y size tree

let () =
    Graphics.open_graph (" "^"800"^"x"^"800");
    draw_square 200 200 50;
    draw_box 400 200 100 "Message";
    draw_tree_node 200 600 50 (fun x -> x) (Nil);
    draw_tree_node 400 600 50 (fun x -> x) (Node ("42", Nil, Nil));
    draw_tree_node 300 400 40 string_of_int (Node (42, Nil, Nil));
    draw_tree_node 500 400 40 string_of_bool (Node (true, Nil, Nil));
    ignore (Graphics.read_key ())
