type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let rec size tree =
    match tree with
    | Nil -> 0
    | Node (_, left, right) -> 1 + (size left) + (size right)

let rec height tree =
    match tree with
    | Nil -> 0
    | Node (_, left, right) -> 1 + (max (size left) (size right))

let rec is_bst tree =
    match tree with
    | Nil -> true
    | Node (v, left, right) ->
        (is_bst left && match left with
                        | Nil -> true
                        | Node (x, _, _) -> x < v)
        && (is_bst right && match right with
                        | Nil -> true
                        | Node (y, _, _) -> y > v)

let rec is_perfect tree =
    match tree with
	| Nil | Node (_, Nil, Nil) -> true
	| Node (_, Nil, _) | Node(_, _, Nil) -> false
	| Node (v, left, right) ->
            is_perfect left && is_perfect right && height left = height right

let rec is_balanced tree =
    match tree with
    | Nil -> true
    | Node (v, left, right) -> is_balanced left && is_balanced right &&
        abs (height left - height right) <= 1

let rec search_bst v tree =
    match tree with
    | Nil -> false
    | Node(i, left, right) when v = i -> true
    | Node(i, left, _) when v < i -> search_bst v left
    | Node(i, _, right) -> search_bst v right

let rec add_bst v tree =
    match tree with
    | Nil -> Node(v, Nil, Nil)
    | Node(i, left, right) when v = i -> failwith "this node already exist"
    | Node(i, left, right) when v < i -> Node(i, add_bst v left, right)
    | Node(i, left, right) -> Node(i, left, add_bst v right)

let rec delete_bst v tree =
    let rec max_bst tree =
        match tree with
        | Nil -> failwith "Error"
        | Node (i, _, Nil) -> i
        | Node (_, _, right) -> max_bst right
    in
    match tree with
    | Nil -> failwith "The total tree have been deleted"
    | Node (i, Nil, Nil) when v = i -> Nil
    | Node (i, left, Nil) when v = i -> left
    | Node (i, Nil, right) when v = i -> right
    | Node (i, left, right) when v = i -> Node (max_bst right, left, delete_bst (max_bst right) right)
    | Node (i, left, right) when v < i -> Node (i, delete_bst v left, right)
    | Node (i, left, right) -> Node (i, left, delete_bst v right)

let () =
    let print_bool v = match v with true -> print_endline "True" | false -> print_endline "False"
    in
    let rec print_tree tree =
        match tree with
        | Nil -> print_endline "."
        | Node (v, left, right) ->
                print_endline (string_of_int v);
                print_tree left; print_tree right
    in
    let n5 = Node (22, Nil, Nil) in
    let n4 = Node (3, Nil, Nil) in
    let n3 = Node (11, n5, Nil) in
    let n2 = Node (5, n4, Nil) in
    let n1 = Node (8, n2, n3)
    in
    print_bool (is_bst n1);
    print_bool (is_perfect n1);
    print_bool (is_balanced n1);
    print_bool (search_bst 3 n1);
    print_endline "----------------------------------";
    print_endline "Standard tree ::";
    print_tree n1;
    print_endline "Adding a node with value = 7 ::";
    print_tree (add_bst 7 n1);
    print_endline "Deleteing a node with value = 3 ::";
    print_tree (delete_bst 3 n1)

