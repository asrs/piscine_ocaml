(* ex01.ml *)

module StringAddon =
    struct
        type t = string

        let equal x y = (String.compare x y) = 0

        let hash str =
            let rec loop i len str acc =
                match i with
                | x when x = len -> acc
                | x -> loop (succ i) len str ((acc * 33) + (int_of_char ((String.get str i))))
            in
            loop 0 (String.length str) str 5391
    end

module StringHashtbl = Hashtbl.Make(StringAddon)

let () =
    let ht = StringHashtbl.create 5 in
    let values = [ "Hello"; "world"; "42"; "Ocaml"; "H" ] in
    let pairs = List.map (fun s -> (s, String.length s)) values in
    List.iter (fun (k,v) -> StringHashtbl.add ht k v) pairs;
    StringHashtbl.iter (fun k v -> Printf.printf "k = \"%s\", v = %d\n" k v) ht
