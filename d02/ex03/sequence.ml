let sequence n =
    let rec lst_to_str lst str =
        match lst with
        | [] -> str
        | h :: t -> lst_to_str t (str ^ (string_of_int h))
    in
    let encode lst =
        let rec do_enc lst n ret =
            match lst with
            | [] -> ret
            | h :: s :: t  -> (
                if h = s then
                    do_enc (s :: t) (n + 1) ret
                else
                    do_enc (s :: t) 0 (ret @ [(n + 1)] @ [h])
                )
            | h :: t  -> do_enc [] 0 (ret @ [(n + 1)] @ [h])
        in
        do_enc lst 0 []
    in
    let rec loop i n lst =
        if i < n then
            loop (i + 1) n (encode lst)
        else
            lst
    in
    if n <= 0 then ""
    else lst_to_str (loop 0 (n - 1) (1 :: [])) ""

let () =
  Printf.printf "sequence -1:\n%s\n" (sequence (-1));
  assert (sequence (-1) = "");
  Printf.printf "sequence 0:\n%s\n" (sequence 0);
  assert (sequence 0 = "");
  Printf.printf "sequence 1:\n%s\n" (sequence 1);
  assert (sequence 1 = "1");
  Printf.printf "sequence 2:\n%s\n" (sequence 2);
  assert (sequence 2 = "11");
  Printf.printf "sequence 3:\n%s\n" (sequence 3);
  assert (sequence 3 = "21");
  Printf.printf "sequence 4:\n%s\n" (sequence 4);
  assert (sequence 4 = "1211");
  Printf.printf "sequence 5:\n%s\n" (sequence 5);
  assert (sequence 5 = "111221");
  Printf.printf "sequence 6:\n%s\n" (sequence 6);
  assert (sequence 6 = "312211");
  Printf.printf "sequence 7:\n%s\n" (sequence 7);
  assert (sequence 7 = "13112221");
