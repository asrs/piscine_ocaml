let rec ft_prefix c lst ret =
    match lst with
    | [] -> ret
    | h :: t -> ft_prefix c t (ret @ [(c ^ h)])

let rec ft_revlist lst ret =
    match lst with
    | [] -> ret
    | h :: t -> ft_revlist t (h :: ret)

let ft_encode acc =
    let lrev = ft_revlist acc []
    in
    let la = (ft_prefix "0" acc [])
    in
    let lb = (ft_prefix "1" lrev [])
    in
    la @ lb

let rec print_gray lst =
    match lst with
    | [] -> print_endline ""
    | h :: s :: t -> (
        print_string (h ^ " ");
        print_gray (s :: t)
    )
    | h :: t -> (
        print_string h;
        print_gray t
    )

let gray n =
    let rec loop i n acc =
        if i >= n then print_gray acc
        else
            loop (succ i) n (ft_encode acc)
    in
    if n <= 0 then ()
    else
        loop 0 (pred n) ["0";"1"]

let () =
    gray (-1);
    gray 0;
    gray 1;
    gray 2;
    gray 3;
    gray 4
