let crossover la lb =
    let rec loop acc la lb =
        match la, lb with
        | [], _ -> acc
        | _ , [] -> acc
        | ha :: ta, hb :: tb -> (
            if ha = hb then loop (acc @ [ha]) ta tb
            else loop acc ta lb
        )
    in
    loop [] la lb

let () =
    let rec print_lst lst =
        match lst with
        | [] -> print_endline ""
        | h :: t -> print_int h; print_string " "; print_lst t
    in
    print_lst (crossover [1;2;3] []);
    print_lst (crossover [1;2;2;3] [1;2;3]);
    print_lst (crossover [] [1;2;3]);
    print_lst (crossover [1;2;3] [1;2;3]);
    print_lst (crossover [1;2;4] [1;2;5]);
    print_lst (crossover [1;2;3] [1;2;3;4;5;6;7]);
    print_lst (crossover [1;2;3;4;5;6;7] [1;2;3]);
