(* galifrey.ml *)

class galifrey dalek doctor people =
    object (self)

        val dalek_army = dalek
        val doctor_army = doctor
        val people_army = people

        method do_time_war =
            let verbose (doc : Doctor.doctor) (dalek : Dalek.dalek) = (
                dalek#talk;
                doc#use_sonic_screwdriver;
                dalek#die
            )
            in
            let rec kill_dalek i x =
                if i = x then print_endline "all Dalek died in vain"
                else (
                    verbose (List.nth doctor_army 0) (List.nth dalek_army 0);
                    kill_dalek (succ i) x
                )
            in
            if (List.length dalek_army < 1 || List.length doctor_army < 1 ||
            List.length people_army < 1) then 
                print_endline "not enought participant for the war"
            else begin
                kill_dalek 0 (List.length dalek_army);
                print_endline "One more time the Doctors save the world !";
                print_endline "Bye bye les daleks."
            end

    end

