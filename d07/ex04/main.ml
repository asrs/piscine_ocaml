(* main.ml *)

let () =
    Random.self_init ();
    let bill = new People.people "Bill Potts" in
    let nardole = new People.people "Nardole" in
    let doctor = new Doctor.doctor "Who" 25 nardole in
    let rec create_dalek i len acc =
        match i with
        | x when x = len -> acc
        | _ -> create_dalek (succ i) len (new Dalek.dalek :: acc)
    in
    let army_dalek = create_dalek 0 ((Random.int 15) + 1) []
    in
    let galifrey =
        new Galifrey.galifrey army_dalek (doctor :: []) (bill :: nardole :: [])
    in
    galifrey#do_time_war

