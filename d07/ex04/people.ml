(* people.ml *)

class people name =
    object
        initializer print_endline "Freshly created !"

        val name : string = name
        val hp : int = 100

        method to_string =
            "My name is " ^ name ^ " and I've got " ^ (string_of_int hp) ^ " hp."

        method talk =
            print_endline ("I'm " ^ name ^ "! Do you know the Doctor ?")

        method die =
            print_endline "Aaaarghh!"

    end
