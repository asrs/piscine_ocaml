(* dalek.ml *)

class dalek =
    object
        initializer Random.self_init ()

        val mutable hp : int = 100
        val mutable shield : bool = true
        val name : string =
                match Random.int 4 with
                | 0 -> "DalekFir"
                | 1 -> "DalekSec"
                | 2 -> "DalekThi"
                | _ -> "DalekFou"

        method to_string =
            "My name is " ^ name ^ " and I've got " ^ (string_of_int hp) ^ " hp. Is my shield activated ? " ^
            match shield with | true -> "Yes it is !" | false -> "Oh no it's not activated..."

        method talk =
            let sentence =
                match Random.int 4 with
                | 0 -> "Explain! Explain!"
                | 1 -> "Exterminate! Exterminate!"
                | 2 -> "I obey!"
                | _ -> "You are the Doctor! You are the enemy of the Daleks!"
            in print_endline sentence

        method exterminate (target : People.people) =
            shield <- not shield;
            target#die

        method die = print_endline "Emergency Temporal Shift!"

    end
