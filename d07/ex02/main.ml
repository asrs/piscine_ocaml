(* main.ml *)

let () =
    let dalek = new Dalek.dalek in
    let bill = new People.people "Bill Potts" in
    let nardole = new People.people "Nardole" in
    let doctor = new Doctor.doctor "Who" 25 nardole
    in
    bill#talk;
    nardole#talk;
    doctor#talk;
    dalek#talk;
    dalek#exterminate bill;
    doctor#use_sonic_screwdriver;
    doctor#travel_in_time 1450 1980;
    dalek#die;
    print_endline nardole#to_string
