(* main.ml *)

let () =
    let d1 = new Dalek.dalek in
    let d2 = new Dalek.dalek in
    let d3 = new Dalek.dalek in
    let d4 = new Dalek.dalek in
    let d5 = new Dalek.dalek in
    let army = new Army.army [d1; d2; d3; d4;d5]
    in
    army#delete;
    army#add (new Dalek.dalek);
    print_endline "I've add a dalek, and del one."

