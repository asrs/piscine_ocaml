(* main.ml *)

let () =
    let bill = new People.people "Bill Potts"
    in
    print_endline bill#to_string;
    bill#talk;
    let doctor = new Doctor.doctor "Who" 25 bill
    in
    print_endline doctor#to_string;
    doctor#talk;
    doctor#travel_in_time 1450 1960;
    bill#die
