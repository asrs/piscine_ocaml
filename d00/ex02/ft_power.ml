let rec ft_power x y =
    if y = 0 then 1
    else if y = 1 then x
    else if y mod 2 = 0 then ft_power (x * x) (y / 2)
    else (ft_power x (y - 1)) * x

let () =
    let prt x y =
        print_int (ft_power x y);
        print_char '\n'
    in
    prt 2 4;
    prt 3 0;
    prt 0 5;
    prt 2 3
