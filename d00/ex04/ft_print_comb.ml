let ft_print_comb () =
    let rec do_comb a b c =
        print_string ", ";
        print_int a;
        print_int b;
        print_int c;
        if a = 7 && b = 8 && c = 9 then ()
        else if b = 8 && c = 9 then do_comb (a + 1) (a + 2) (a + 3)
        else if c = 9 then do_comb a (b + 1) (b + 2)
        else do_comb a b (c + 1)
    in
    print_int 0;
    print_int 1;
    print_int 2;
    do_comb 0 1 3;
    print_string "\n"

let () =
    ft_print_comb ()
