let ft_rot_n n s =
    let do_rot c =
        let alpha =
            if c >= 'a' && c <= 'z' then 
                int_of_char 'a'
            else
                int_of_char 'A'
        in
        if c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' then
            char_of_int((int_of_char c - alpha + n) mod 26 + alpha)
        else c
    in
    String.map do_rot s

let () =
    print_endline (ft_rot_n 1 "abcdefghijklmnopqrstuvwxyz");
    print_endline (ft_rot_n 13 "abcdefghijklmnopqrstuvwxyz");
    print_endline (ft_rot_n 42 "0123456789");
    print_endline (ft_rot_n 2 "OI2EAS67B9");
    print_endline (ft_rot_n 0 "Damned !");
    print_endline (ft_rot_n 42 "");
    print_endline (ft_rot_n 1 "NBzlk qnbjr !")
