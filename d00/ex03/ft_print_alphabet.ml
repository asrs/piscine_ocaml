let ft_print_alphabet () =
    let rec browse_alpha a b =
        if a = b then print_char a
        else if a < b then (
            print_char a;
            browse_alpha (char_of_int((int_of_char a) + 1)) b
            )
        else ()
    in
    browse_alpha 'a' 'z';
    print_char '\n'

let () =
    ft_print_alphabet ()
