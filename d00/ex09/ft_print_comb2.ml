let ft_print_comb2 () =
    let rec do_comb a b =
        print_char ',';
        print_char ' ';
        if a < 10 then (
            print_int 0;
            print_int a;
        )
        else print_int a;
        print_char ' ';
        if b < 10 then (
            print_int 0;
            print_int b;
        )
        else print_int b;
        if a = 98 && b = 99 then ()
        else if b = 99 then do_comb (a + 1) (a + 2)
        else do_comb a (b + 1)
    in
    print_int 0;
    print_int 0;
    print_char ' ';
    print_int 0;
    print_int 1;
    do_comb 0 2;
    print_char '\n'

let () =
    ft_print_comb2 ()
