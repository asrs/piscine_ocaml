let ft_string_all f s =
    let l = String.length s
    in
    let rec loop i =
        if i < l then
            f (String.get s i) && loop (i + 1)
        else true
    in
    loop 0

let () =
    let is_digit c = c >= '0' && c <= '9'
    in
    if ft_string_all is_digit "0123456789" then
        print_endline "True"
        else
            print_endline "False";
    if ft_string_all is_digit "01234B6789" then
        print_endline "True"
    else
        print_endline "False"
