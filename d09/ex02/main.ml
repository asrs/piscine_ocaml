
module Calc_int = Monoid.Calc (Monoid.INT)
module Calc_float = Monoid.Calc (Monoid.FLOAT)

let () =
    print_endline "pow 3 3 int/float";
    print_endline (string_of_int (Calc_int.power 3 3));
    print_endline (string_of_float (Calc_float.power 3.0 3));
    print_endline "mul/add (20 + 1) * 2  int/float";
    print_endline (string_of_int (Calc_int.mul (Calc_int.add 20 1) 2));
    print_endline (string_of_float (Calc_float.mul (Calc_float.add 20.0 1.0) 2.0));
    print_endline "mul/add (20 * 3) - 2  int/float";
    print_endline (string_of_int (Calc_int.mul (Calc_int.sub 20 3) 2));
    print_endline (string_of_float (Calc_float.mul (Calc_float.sub 20.0 3.0) 2.0));
    print_endline "div 20 / 2  int/float";
    print_endline (string_of_int (Calc_int.div 20 2));
    print_endline (string_of_float (Calc_float.div 20.0 2.0));
    print_endline "mul/add (20 * 3) - 2  int/float";
    print_endline (string_of_int (Calc_int.fact 5 ));
    print_endline (string_of_float (Calc_float.fact 5.))
