
let print_proj (a : App.App.project) : unit =
    let str = a.product ^ " | " ^ a.status ^ " | " ^ (string_of_int a.grade)
    in
    print_endline str

let () =
    let p1 : App.App.project = (App.App.zero) in
    let p2 : App.App.project = {product = "Rush 00"; status = "succeed"; grade = 80} in
    let p3 : App.App.project = {product = "Rush 01"; status = "succeed"; grade = 120}
    in
    print_endline "Test App.App.fail with App.App.zero : ";
    print_proj (App.App.fail p1);
    print_endline "";
    print_endline "Test App.App.success with App.App.zero : ";
    print_proj (App.App.success p1);
    print_endline "";
    print_endline "Test App.App.fail with p2";
    print_proj (App.App.fail p2);
    print_endline "";
    print_endline "Test App.App.success with p2";
    print_proj (App.App.success p2);
    print_endline "";
    print_endline "Test App.App.combine with p1 & p2";
    print_proj (App.App.combine p1 p2);
    print_endline "";
    print_endline "Test App.App.combine with p2 & p3";
    print_proj (App.App.combine p2 p3)
