(* App.ml *)

module type App =
    sig
        type project
        val zero : project
        val combine : project -> project -> project -> project
        val fail : project -> project -> project
        val success : project -> project -> project
    end

module App =
    struct
        type project = {product : string; status : string; grade : int}

        let zero = {product = "" ; status = "" ; grade = 0}

        let combine a b =
            let get_product a b = (a.product ^ " + " ^ b.product) in
            let get_grade a b = (a.grade + b.grade) / 2 in
            let get_status x =
                match x with
                | x when x >= 80 -> "succeed"
                | _ -> "failed"
            in
            {
                product = get_product a b;
                status = get_status (get_grade a b);
                grade = get_grade a b;
             }

        let fail a =
            {
                product = a.product;
                status = "failed";
                grade = 0;
            }

        let success a =
            {
                product = a.product;
                status = "suceed";
                grade = 80; 
            }

    end
