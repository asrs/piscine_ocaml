(* Watchtower.ml *)

module type Watchtower =
    sig
        type hour
        val zero : hour
        val add : hour -> hour -> hour
        val sub : hour -> hour -> hour
    end

module Watchtower =
    struct
        type hour = int

        let zero : hour = 12

        let add x y : hour =
            match ((x + y) mod 12) with
            | 0 -> zero
            | _ -> ((x + y) mod 12)

        let sub x y : hour =
            match ((x - y) mod 12) with
            | 0 -> zero
            | a when a < 0 -> ((x - y) mod 12) * (-1)
            | _ -> ((x - y) mod 12)
    end
