module Color =
struct
        type t = Spade | Heart | Diamond | Club

    let all : t list = Spade :: Heart :: Diamond :: Club :: []

    let toString (x : t) : string = match x with
    | Spade -> "S"
    | Heart -> "H"
    | Diamond -> "D"
    | Club -> "C"

    let toStringVerbose (x : t) : string = match x with
    | Spade -> "Spade"
    | Heart -> "Heart"
    | Diamond -> "Diamond"
    | Club -> "Club"
end

module Value =
struct
    type t = T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | Jack | Queen | King | As

    let all = T2 :: T3 :: T4 :: T5 :: T6 :: T7 :: T8 :: T9 :: T10 :: Jack :: Queen :: King :: As :: []

    let toInt x =
        match x with
    | T2 -> 1
    | T3 -> 2
    | T4 -> 3
    | T5 -> 4
    | T6 -> 5
    | T7 -> 6
    | T8 -> 7
    | T9 -> 8
    | T10 -> 9
    | Jack -> 10
    | Queen -> 11
    | King -> 12
    | As -> 13

    let toString x =
        match x with
    | T2 -> "2"
    | T3 -> "3"
    | T4 -> "4"
    | T5 -> "5"
    | T6 -> "6"
    | T7 -> "7"
    | T8 -> "8"
    | T9 -> "9"
    | T10 -> "10"
    | Jack -> "J"
    | Queen -> "Q"
    | King -> "K"
    | As -> "A"

    let toStringVerbose x = match x with
    | T2 -> "2"
    | T3 -> "3"
    | T4 -> "4"
    | T5 -> "5"
    | T6 -> "6"
    | T7 -> "7"
    | T8 -> "8"
    | T9 -> "9"
    | T10 -> "10"
    | Jack -> "Jack"
    | Queen -> "Queen"
    | King -> "King"
    | As -> "As"

    let next x = match x with
    | T2 -> T3
    | T3 -> T4
    | T4 -> T5
    | T5 -> T6
    | T6 -> T7
    | T7 -> T8
    | T8 -> T9
    | T9 -> T10
    | T10 -> Jack
    | Jack -> Queen
    | Queen -> King
    | King -> As
    | As -> invalid_arg "Can not give the next card"

    let previous x = match x with
    | T2 -> invalid_arg "Can not give the previous card"
    | T3 -> T2
    | T4 -> T3
    | T5 -> T4
    | T6 -> T5
    | T7 -> T6
    | T8 -> T7
    | T9 -> T8
    | T10 -> T9
    | Jack -> T10
    | Queen -> Jack
    | King -> Queen
    | As -> King

end

module Card =
struct
    type t = Value.t * Color.t

    let newCard v c : t = (v, c)

    let allSpades = List.map (fun x -> (newCard x Color.Spade)) Value.all

    let allHearts = List.map (fun x -> (newCard x Color.Heart)) Value.all

    let allDiamonds = List.map (fun x -> (newCard x Color.Diamond)) Value.all

    let allClubs = List.map (fun x -> (newCard x Color.Club)) Value.all

    let all = List.concat [allSpades;allHearts;allDiamonds;allClubs]

    let getValue ((v, c) : t) = v

    let getColor ((v, c) : t) = c

    let toString ((v, c) : t) : string =
        Printf.sprintf ("%s%s") (Value.toString v) (Color.toString c)

    let toStringVerbose ((v, c) : t) : string =
        Printf.sprintf ("Card(%s, %s)") (Value.toStringVerbose v) (Color.toStringVerbose c)

    let compare (a,_) (b,_) : int = (Value.toInt a) - (Value.toInt b)

    let max (e1 : t) (e2 : t) : t =
        if compare e1 e2 < 0 then e1
    else e2

    let min (e1 : t) (e2 : t) : t =
        if compare e1 e2 > 0 then e1
    else e2

    let best (lst : t list) : t = 
        match lst with
    | [] -> invalid_arg "List is empty"
    | h :: t -> List.fold_left max h t

    let isOf ((v, c) : t) (color : Color.t) : bool =
        if c = color then true
    else false

    let isSpade ((v,c) : t) : bool =
        if c = Color.Spade then true
    else false

    let isHeart ((v,c) : t) : bool =

        if c = Color.Heart then true
    else false

    let isDiamond ((v,c) : t) : bool =
        if c = Color.Diamond then true
    else false

    let isClub ((v,c) : t) : bool =
        if c = Color.Club then true
    else false
end

type t = Card.t list

let newDeck =
    Random.self_init ();
    let shuffle lst =
        let nd = List.map (fun c -> (Random.bits (), c)) lst (* associate random number to card *)
        in
        let sond = List.sort compare nd (* Now sorting by comparing them number *)
        in
        let get_one sond =
            match sond with
            | (i, c) -> c
        in
        List.map get_one sond
        in shuffle Card.all

let toStringList deck =
    let rec loop deck =
        match deck with
        | [] -> []
        | h :: t -> Card.toString h :: loop t
    in
    loop deck

let toStringListVerbose deck =
    let rec loop deck =
        match deck with
        | [] -> []
        | h :: t -> Card.toStringVerbose h :: loop t
    in
    loop deck

let drawCard deck =
    match deck with
    | [] -> failwith "The Deck is empty"
    | h :: t -> (h, t)

