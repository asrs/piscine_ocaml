type t = Spade | Heart | Diamond | Club

let all : t list = Spade :: Heart :: Diamond :: Club :: []

let toString (x : t) : string =
    match x with
    | Spade -> "S"
    | Heart -> "H"
    | Diamond -> "D"
    | Club -> "C"

let toStringVerbose (x : t) : string =
    match x with
    | Spade -> "Spade"
    | Heart -> "Heart"
    | Diamond -> "Diamond"
    | Club -> "Club"
