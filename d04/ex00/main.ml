
let () =
    let test =
        Color.Spade :: Color.Spade :: Color.Club :: Color.Diamond
        :: Color.Diamond :: Color.Diamond :: Color.Spade :: Color.Heart :: []
    in
    let rec loop lst =
        match lst with
        | [] -> ()
        | h :: t -> (
            print_string (Color.toString h);
            print_string " : ";
            print_endline (Color.toStringVerbose h); 
            loop t
        )
    in (
        loop Color.all;
        print_endline "";
        loop test
    )
