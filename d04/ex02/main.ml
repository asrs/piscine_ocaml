let testColor () =
    print_endline "***** COLOR --- TEST ";
    let test =
        Card.Color.Spade :: Card.Color.Spade :: Card.Color.Club :: Card.Color.Diamond
        :: Card.Color.Diamond :: Card.Color.Diamond :: Card.Color.Spade :: Card.Color.Heart :: []
    in
    let rec loop lst =
        match lst with
        | [] -> ()
        | h :: t -> (
            print_string (Card.Color.toString h);
            print_string " : ";
            print_endline (Card.Color.toStringVerbose h); 
            loop t
        )
    in (
        loop Card.Color.all;
        print_endline "";
        loop test;
        print_endline ""
        )

let testValue () =
    print_endline "****** VALUE --- TEST ";
    let rec loop lst =
        match lst with
        | [] -> ()
        | h :: t -> (
            print_string "Card = ";
            print_string (Card.Value.toString h);
            print_string " : ";
            print_string (Card.Value.toStringVerbose h);
            print_string " : Previous -> ";
            if h != Card.Value.T2 then 
                print_string (Card.Value.toString (Card.Value.previous h))
            else
                print_string ("No previous available");
            print_string " : Next -> ";
            if h != Card.Value.As then
                print_string (Card.Value.toStringVerbose (Card.Value.next h))
            else
                print_string ("No next available");
            print_endline "";
            loop t
        )
        in
        loop Card.Value.all;
        print_endline ""

let testCard () =
    print_endline "****** Card --- TEST ";
    let rec loop lst =
        match lst with
        | [] -> ()
        | x :: xs -> ( print_endline @@ Card.toString x;
                   print_endline @@ Card.toStringVerbose x;
                   loop xs)
    in
    print_endline "Display all cards (short and verbose):";
    loop Card.all;
    let a = Card.newCard Card.Value.T4 Card.Color.Heart
    in
    let b = Card.newCard Card.Value.T6 Card.Color.Spade
    in
    print_endline "\nLet a = 4H and b = 6S";
    print_endline "\ncompare a b:";
    print_endline @@ string_of_int @@ Card.compare a b;

    print_endline "\nmax a b";
    print_endline @@ Card.toString @@ Card.max a b;

    print_endline "\nmin a b";
    print_endline @@ Card.toString @@ Card.min a b;

    print_endline "\nbest [a;b]:";
    print_endline @@ Card.toString @@ Card.best [a; b];

    print_endline "\ngetValue a:";
    print_endline @@ Card.Value.toString @@ Card.getValue @@ a;

    print_endline "getColor a:";
    print_endline @@ Card.Color.toString @@ Card.getColor @@ a

let () =
    testColor ();
    testValue ();
    testCard ()
