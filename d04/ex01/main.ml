
let () =
    let rec loop lst =
        match lst with
        | [] -> ()
        | h :: t -> (
            print_string "Card = ";
            print_string (Value.toString h);
            print_string " : ";
            print_string (Value.toStringVerbose h);
            print_string " : Previous -> ";
            if h != Value.T2 then 
                print_string (Value.toString (Value.previous h))
            else
                print_string ("No previous available");
            print_string " : Next -> ";
            if h != Value.As then
                print_string (Value.toStringVerbose (Value.next h))
            else
                print_string ("No next available");
            print_endline "";
            loop t
        )
    in
    loop Value.all;
    print_endline "";
    print_string (Value.toString (Value.previous Value.T2));
    print_string (Value.toString (Value.next Value.As))
