let rec iter f x n =
    if n < 0 then -1
    else if n == 0 then x
    else iter f (f x) (n - 1)

let () =
    let print_rec f x n =
        print_int (iter f x n);
        print_char '\n'
    in
    print_rec (fun x -> x * x) 2 (-1);
    print_rec (fun x -> x * x) 2 4;
    print_rec (fun x -> x * 2) 2 4;
    print_rec (fun x -> x) 2 100000;
    print_rec (fun x -> x * 2) 2 0
