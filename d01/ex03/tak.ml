let rec tak x y z =
    if y < x then
        tak (tak (x - 1) y z) (tak (y - 1) z x) (tak (z - 1) x y)
    else z

let () =
    let print_rec x y z =
        print_int (tak x y z);
        print_char '\n'
    in
    print_rec 1 2 3;
    print_rec 5 23 7;
    print_rec 9 1 0;
    print_rec 1 1 1;
    print_rec 0 42 0;
    print_rec 23498 98734 98776
