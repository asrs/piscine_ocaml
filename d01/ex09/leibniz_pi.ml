let rec leibniz_pi (delta : float) =
    let r_pi = (4. *. (atan 1.))
    in
    let ft_sign x = if x mod 2 = 0 then 1. else (-1.)
    in
    let ft_expr x = (ft_sign x) /. (float_of_int ((2 * x) + 1))
    in
    let ft_abs x = if x < 0. then (-.x) else x
    in
    let rec loop (i : int) (acc : float) =
        if ft_abs((r_pi -. (4. *. acc))) <= delta then i
        else loop (i + 1) (acc +. (ft_expr i))
    in
    if delta < 0. then (-1)
    else loop 0 0.

let () =
  Printf.printf "leibniz_pi 0.001:\n%d\n" (leibniz_pi 0.001);
  assert (leibniz_pi 0.001 == 1000);
  Printf.printf "leibniz_pi 1.:\n%d\n" (leibniz_pi 1.);
  assert (leibniz_pi 1. == 1);
  Printf.printf "leibniz_pi (-0.1):\n%d\n" (leibniz_pi (-0.1));
  assert (leibniz_pi (-0.1) == (-1))

