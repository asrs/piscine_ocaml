let repeat_x n =
    let str = ""
    in
    let rec do_repeat s n =
        if n < 0 then "Error"
        else if n = 0 then s
        else do_repeat (s ^ "x") (n - 1)
    in
    do_repeat str n

let () =
    print_endline (repeat_x (-1));
    print_endline (repeat_x 1);
    print_endline (repeat_x 2);
    print_endline (repeat_x 5)
