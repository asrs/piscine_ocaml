let fibonacci n =
    let rec loop n a b =
    if n < 0 then -1
    else if n = 0 then a
    else if n = 1 then b
    else (loop (n - 1) b (a + b))
    in
    loop n 0 1

let () =
    let print_ret n =
        print_int (fibonacci n);
        print_char '\n'
    in
    print_ret (-42);
    print_ret 1;
    print_ret 3;
    print_ret 6;
    print_ret 9;
    print_ret 15;
    print_ret 1000
