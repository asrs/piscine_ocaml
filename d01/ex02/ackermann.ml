let rec ackermann m n =
    if m < 0 || n < 0 then (-1)
    else if m = 0 then (n + 1)
    else if m > 0 && n = 0 then ackermann (m - 1) 1
    else ackermann (m - 1) (ackermann m (n - 1))

let () =
    let print_result m n =
        print_int (ackermann m n);
        print_char '\n'
    in
    print_result (-1) 7;
    print_result 0 0;
    print_result 0 10000;
    print_result 2 3;
    print_result 4 1
