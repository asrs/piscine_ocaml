let repeat_string ?(str = "x") n =
    let ret = ""
    in
    let rec do_repeat ret str n =
        if n < 0 then "Error"
        else if n = 0 then ret
        else do_repeat (ret ^ str) str (n - 1)
    in
    do_repeat ret str n

let () =
    print_endline (repeat_string (-1));
    print_endline (repeat_string 0);
    print_endline (repeat_string ~str:"Toto" 1);
    print_endline (repeat_string 2);
    print_endline (repeat_string ~str:"a" 5);
    print_endline (repeat_string ~str:"what" 3)
