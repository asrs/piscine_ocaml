let ft_sum fn i limit =
    let rec loop fn i limit acc =
        if limit < i then nan
        else if i = limit then (acc +. (fn i))
        else loop fn (i + 1) limit (acc +. (fn i))
    in
    loop fn i limit 0.

let () =
    print_float (ft_sum (fun i -> float_of_int (i * i)) 1 10);
    print_char '\n';
    print_float (ft_sum (fun i -> float_of_int (i * i)) 10 10);
    print_char '\n';
    print_float (ft_sum (fun i -> float_of_int (i * i)) 99 10);
    print_char '\n';
    print_float (ft_sum (fun i -> float_of_int (i * i)) 1 10000);
    print_char '\n'
