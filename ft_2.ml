let ft_print_alphabet () =
    let rec browse_alpha a b =
        match a with
        | _ when a < b ->
            print_char a;
            browse_alpha (char_of_int((int_of_char a) + 1)) b
        | _ when a > b -> ()
        | b -> print_char a
    in
    browse_alpha 'a' 'z';
    print_char '\n'

let () = ft_print_alphabet ()
