let () =
    let print_eq n =
        print_int n;
        print_char '\n'
    in
    let hydrogen = new Atom_element.hydrogen in
    let helium = new Atom_element.helium in
    let lithium = new Atom_element.lithium in
    let berylium = new Atom_element.berylium in
    let boron = new Atom_element.boron in
    let carbon = new Atom_element.carbon in
    let nitrogen = new Atom_element.nitrogen in
    let oxygen = new Atom_element.oxygen
    in
    let water = new Molecule_element.water in
    let carbon_dioxyde = new Molecule_element.carbon_dioxyde in
    let trinitrotoluene = new Molecule_element.trinitrotoluene in
    let acetate = new Molecule_element.acetate in
    let strychnine = new Molecule_element.strychnine
    in
    let methane = new Alkane_element.methane in
    let ethane = new Alkane_element.ethane in
    let propane = new Alkane_element.propane in
    let butane = new Alkane_element.butane in
    let pentane = new Alkane_element.pentane in
    let hexane = new Alkane_element.hexane in
    let heptane = new Alkane_element.heptane in
    let octane = new Alkane_element.octane in
    let nonane = new Alkane_element.nonane in
    let dodecane = new Alkane_element.dodecane
    in
    print_endline "#### Atoms:";
    print_endline (hydrogen#to_string);
    print_endline (helium#to_string);
    print_endline (lithium#to_string);
    print_endline (berylium#to_string);
    print_endline (boron#to_string);
    print_endline (carbon#to_string);
    print_endline (nitrogen#to_string);
    print_endline (oxygen#to_string);
    print_endline "#### Molecule:";
    print_endline (water#to_string);
    print_endline (carbon_dioxyde#to_string);
    print_endline (trinitrotoluene#to_string);
    print_endline (strychnine#to_string);
    print_endline (acetate#to_string);
    print_endline "#### Alkanes:";
    print_endline (methane#to_string);
    print_endline (ethane#to_string);
    print_endline (propane#to_string);
    print_endline (butane#to_string);
    print_endline (pentane#to_string);
    print_endline (hexane#to_string);
    print_endline (heptane#to_string);
    print_endline (octane#to_string);
    print_endline (nonane#to_string);
    print_endline (dodecane#to_string);
    print_endline "---------------------------------------";
    print_endline hydrogen#name;
    print_endline hydrogen#symbol;
    print_int hydrogen#atomic_number;
    print_char '\n';
    print_eq (oxygen#equals hydrogen);
    print_eq (hydrogen#equals hydrogen);
    print_eq (hydrogen#equals oxygen);
    print_endline water#name;
    print_endline water#formula;
    print_eq (water#equals water);
    print_eq (water#equals acetate);
    print_eq (water#equals strychnine);
    print_endline nonane#name;
    print_endline nonane#formula;
    print_eq (propane#equals nonane);
    print_eq (propane#equals propane);
    print_eq (propane#equals methane)


