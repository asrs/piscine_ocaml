(* Reaction.ml *)

class virtual reaction (start : Molecule_class.molecule list) (ending : Molecule_class.molecule list) =
    object (self)

    method virtual get_start: (Molecule_class.molecule * int) list

    method virtual get_result: (Molecule_class.molecule * int) list

    method virtual balance: reaction

    method virtual is_balanced: bool

    end
