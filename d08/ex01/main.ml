let () =
    let print_eq n =
        print_int n;
        print_char '\n'
    in
    let hydrogen = new Atom_element.hydrogen in
    let helium = new Atom_element.helium in
    let lithium = new Atom_element.lithium in
    let berylium = new Atom_element.berylium in
    let boron = new Atom_element.boron in
    let carbon = new Atom_element.carbon in
    let nitrogen = new Atom_element.nitrogen in
    let oxygen = new Atom_element.oxygen
    in
    let water = new Molecule_element.water in
    let carbon_dioxyde = new Molecule_element.carbon_dioxyde in
    let trinitrotoluene = new Molecule_element.trinitrotoluene in
    let acetate = new Molecule_element.acetate in
    let strychnine = new Molecule_element.strychnine
    in
    print_endline (hydrogen#to_string);
    print_endline (helium#to_string);
    print_endline (lithium#to_string);
    print_endline (berylium#to_string);
    print_endline (boron#to_string);
    print_endline (carbon#to_string);
    print_endline (nitrogen#to_string);
    print_endline (oxygen#to_string);
    print_endline (water#to_string);
    print_endline (carbon_dioxyde#to_string);
    print_endline (trinitrotoluene#to_string);
    print_endline (strychnine#to_string);
    print_endline (acetate#to_string);
    print_eq (oxygen#equals hydrogen);
    print_eq (nitrogen#equals hydrogen);
    print_eq (carbon#equals hydrogen);
    print_eq (boron#equals hydrogen);
    print_eq (berylium#equals hydrogen);
    print_eq (lithium#equals hydrogen);
    print_eq (helium#equals hydrogen);
    print_eq (hydrogen#equals hydrogen);
    print_eq (hydrogen#equals oxygen);
    print_endline water#name;
    print_endline water#formula;
    print_endline carbon_dioxyde#name;
    print_endline carbon_dioxyde#formula;
    print_endline trinitrotoluene#name;
    print_endline trinitrotoluene#formula;
    print_endline acetate#name;
    print_endline acetate#formula;
    print_endline strychnine#name;
    print_endline strychnine#formula;
    print_eq (water#equals water);
    print_eq (water#equals acetate);
    print_eq (water#equals strychnine)
