(* Atom_element.ml *)

class hydrogen =
    object (self)
        inherit Atom_class.atom "hydrogen" "H" 1
    end

class helium =
    object (self)
        inherit Atom_class.atom "helium" "He" 2
    end

class lithium =
    object (self)
        inherit Atom_class.atom "lithium" "Li" 3
    end

class berylium =
    object (self)
        inherit Atom_class.atom "berylium" "Be" 4
    end

class boron =
    object (self)
        inherit Atom_class.atom "boron" "B" 5
    end

class carbon =
    object (self)
        inherit Atom_class.atom "carbon" "C" 6
    end


class nitrogen =
    object (self)
        inherit Atom_class.atom "nitrogen" "N" 7
    end

class oxygen =
    object (self)
        inherit Atom_class.atom "oxygen" "O" 8
    end


