let () =
    let print_eq n =
        print_int n;
        print_char '\n'
    in
    let hydrogen = new Atom_element.hydrogen in
    let helium = new Atom_element.helium in
    let lithium = new Atom_element.lithium in
    let berylium = new Atom_element.berylium in
    let boron = new Atom_element.boron in
    let carbon = new Atom_element.carbon in
    let nitrogen = new Atom_element.nitrogen in
    let oxygen = new Atom_element.oxygen
    in
    print_endline (hydrogen#to_string);
    print_endline (helium#to_string);
    print_endline (lithium#to_string);
    print_endline (berylium#to_string);
    print_endline (boron#to_string);
    print_endline (carbon#to_string);
    print_endline (nitrogen#to_string);
    print_endline (oxygen#to_string);
    print_endline (nitrogen#name);
    print_endline (oxygen#symbol);
    print_endline("" ^ string_of_int (oxygen#atomic_number));
    print_endline "";
    print_eq (oxygen#equals hydrogen);
    print_eq (nitrogen#equals hydrogen);
    print_eq (carbon#equals hydrogen);
    print_eq (boron#equals hydrogen);
    print_eq (berylium#equals hydrogen);
    print_eq (lithium#equals hydrogen);
    print_eq (helium#equals hydrogen);
    print_eq (hydrogen#equals hydrogen);
    print_eq (hydrogen#equals oxygen)


