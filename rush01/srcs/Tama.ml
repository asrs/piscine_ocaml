class tamagochi health hygiene energy happiness =
    object (self)
        val health : int = health
        val hygiene : int = hygiene
        val energy : int = energy
        val happiness : int = happiness

        method format_value x =
            match x with
            | a when a < 0 -> 0
            | a when a > 100 -> 100
            | _ -> x

        method get_health =
            self#format_value health

        method get_hygiene =
            self#format_value hygiene

        method get_energy =
            self#format_value energy

        method get_happiness =
            self#format_value happiness

        method is_dead =
            health < 1 || hygiene < 1 || energy < 1 || happiness < 1

        method died =
            "end of the game init"

        method do_eat =
            {< health = (self#format_value (self#get_health + 25)) >}
        method do_bath =
            hygiene
        method do_thunder =
            energy
        method do_kill =
            happiness

        method loose_life =
            "do this action every second"

        method save =
            "svg tama.sv"

        method load =
            "open tama.sv, get value and init element from the file"
    end

