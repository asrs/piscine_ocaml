(* jokes.ml *)

let () =
    let lst = [
        "What did the green grape say to the purple grape?\n  Breathe dammit, BREATHE!";
        "What do you call a fake noodle?\n  An impasta!";
        "Did you hear about the guy who invented the knock knock joke?\n  He won the 'no-bell' prize!";
        "How much does a skeleton weigh?\n  A skele-TON!";
        "Why are there fences around a graveyard?\n  Because people are dying to get in!";
    ]
    in
    Random.self_init ();
    print_endline (List.nth lst (Random.int 5))
