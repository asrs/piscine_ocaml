(* ft_ref.ml *)

type 'a ft_ref = { mutable contents: 'a }

let return n = { contents = n }

let get ref = ref.contents

let set ref n = ref.contents <- n

let bind f (ref : 'a ft_ref) : 'b ft_ref = f (get ref)

let () =
    let value = return 42
    in
    let str = return "lambda"
    in
    let value2 = bind (fun x -> return (x * 2)) value
    in
    print_string "Value = ";
    print_int (get value); print_char '\n';
    print_string "Value2 = ";
    print_int (get value2); print_char '\n';
    print_string "Str = ";
    print_endline (get str);
    print_string "Str after changement = ";
    set str "gamma";
    print_endline (get str);
