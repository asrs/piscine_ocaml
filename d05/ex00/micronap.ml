(* micronap.ml *)

(* Compile with -> ocamlc unix.cma %f *)

let my_sleep () = Unix.sleep 1

let wait n =
    for wait = 1 to n
    do
        print_endline "I'm waiting";
        my_sleep ()
    done

let () =
    match (Array.length Sys.argv) with
    | 2 -> wait (int_of_string Sys.argv.(1))
    | _ -> ()
