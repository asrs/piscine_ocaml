(* jokes.ml *)

let read_file file : string list =
    let file = open_in file in
    let ret = ref []
    in
    try
        while true
        do
            ret := input_line file :: !ret
        done; !ret
    with End_of_file ->
        close_in file;
        !ret

let gen_joke lst =
    Random.self_init ();
    List.nth lst (Random.int 5)

let () =
    match (Array.length Sys.argv) with
    | 2 -> print_endline (gen_joke (read_file Sys.argv.(1)))
    | _ -> print_endline "a.out jokes_file.txt";

