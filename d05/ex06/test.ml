
let grab_file path : string list list =
    let file = open_in path in
    let ret = ref []
    in
    try
        while true do
            ret := !ret @ (
                (String.split_on_char ',' (input_line file)) :: []
            )
        done; !ret
    with End_of_file ->
        close_in file;
        !ret

let () =
    let file = grab_file "./data.csv"
    in
    List.iter (fun x -> List.iter print_endline x) file
